# Postgres replication standby + slot

Vagrant + ansible поднимают 2 виртуальные машины

* Master 192.168.199.137
* Slave 192.168.199.138

Пароль пользователя postgres - 12345678

**На обоих машинах**
1. Устанавливаем postgresql11-server и pg11-contrib (из репозитория Postgres)
2. Устанавливаем пароль для пользователя postgres = 12345678 (генерируем хэш при помощи python)


**На Master**

1. Инициализируем кластер(базу) /usr/pgsql-11/bin/postgresql-11-setup initdb
2. вносим правки в /var/lib/pgsql/11/data/postgresql.conf

```
listen_addresses = 'localhost,192.168.199.137'
wal_level = replica
max_wal_senders = 10
wal_keep_segments = 64
```
3. Добавляем строки в /var/lib/pgsql/11/data/pg_hba.conf  - разрешения на репликацию
```
host	replication 	replicauser 	127.0.0.1/32		trust
host	replication 	replicauser 	192.168.199.137/32 	trust
host 	replication 	replicauser	    192.168.199.138/32 	trust
```
4. Запускаем systemctl start postgresql-11  
5. Создаем пользователя для репликации и создаем новый слот
```
su - postgres 
psql -c "CREATE ROLE replicauser WITH REPLICATION LOGIN ENCRYPTED PASSWORD 'test';"
psql -c "SELECT * FROM pg_create_physical_replication_slot('node_a_slot');"
```

**На Slave**
1. Копируем базу с master 
```
/usr/pgsql-11/bin/pg_basebackup -D /var/lib/pgsql/11/data/ -c fast -X fetch -P -Fp -R -h 192.168.199.137 -p 5432 -U replicauser
подробнее о параметрах - man pg_basebackup
```
2. Вносим правки в /var/lib/pgsql/11/datarecovery.conf
```
standby_mode = 'on'
primary_conninfo = 'user=replicauser passfile=''/var/lib/pgsql/.pgpass'' host=192.168.199.137 port=5432 sslmode=prefer sslcompression=0 krbsrvname=postgres target_session_attrs=any'
primary_slot_name = 'node_a_slot'
trigger_file = '/tmp/MasterNow'
```
3. Включаем hot_standby  в /var/lib/pgsql/11/data/postgresql.conf
```
sed -i 's/#hot_standby = on/hot_standby = on/g' /var/lib/pgsql/11/data/postgresql.conf
sed -i 's/#hot_standby_feedback = off/hot_standby_feedback = on/g' /var/lib/pgsql/11/data/postgresql.conf
```
4. Запускаем systemctl start postgresql-11

# backup при помощи pg_basebackup

Скрипт Бэкапа 
```
#!/bin/bash
d=$(date +"%Y-%m-%d")                                                                                       # Текущая дата

pg_basebackup -D /opt/pg_backup/$d/ -c fast -X fetch -P -Fp -R -h 192.168.199.137 -p 5432 -U replicauser    # Копируем базу в /opt/pg_backup/*current date*

if [ -d /opt/pg_backup/$d ]; then                                                                           # Проверяем - если бэкап появился, то  удаляем все бэкапы старше 8 дней
                                                                                                            
  find /opt/pg_backup/* -type d -mtime +8 -exec rm -rf {} \;

  echo "$d Pg_basebackup  successful " >> /var/log/pg_backup                                                # Пишем в лог, что бэкап прошел успешно
else
  echo "$d Pg_basebackup  failed " >> /var/log/pg_backup                                                    # если папки с бэкапом не появилось, пишем в лог
  mail -s “pg_backup failed” vagrant@slave <<< "$d Pg_basebackup  failed "                                  # а так же отправляем письмо пользователю vagrant
fi
```

**Для работы скрипта необходимо**
от root
1. Установить yum install mailx
2. mkdir /opt/pg_backup
3. touch /var/log/pg_backup
4. добавить задание в CRON на ежедневный запуск `0 0 1 * * /opt/buscript.sh`


